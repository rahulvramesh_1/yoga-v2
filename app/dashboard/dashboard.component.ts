import {Component} from 'angular2/core';
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {ROUTER_DIRECTIVES, RouteConfig, Router, Location, Route} from 'angular2/router';
import * as Rx from 'rxjs/Rx';

@Component({
	selector: 'dashboard',
	templateUrl: './app/dashboard/dashboard.component.html',
	styleUrls: ['./assets/topbar.css'],
	directives:[ROUTER_DIRECTIVES],
	host: { 'class': 'ng-animate page1Container' },
})
export class DashboardComponent{

}